import React from 'react';
import {createNavigator, createStackNavigator} from 'react-navigation';

import CompanyScreen from '../screens/CompanyScreen';
import {ListCompanies} from "../screens/ListCompanies";

export default createStackNavigator({
        ListCompanies: ListCompanies,
        Company: CompanyScreen,
    },
    {
        initialRouteName: 'ListCompanies',
    });