import React from 'react';
import {} from 'react-native';
import {View} from "react-native";
import {Text} from "react-native";

export default class ListCompanies extends React.Component {
    static navigationOptions = ({navigation}) => {
        let company = navigation.getParam('company', null);
        return {
            title: company.company_name || 'Company',
        };
    };

    render() {
        const {navigation} = this.props;
        const company = this.props.navigation.getParam('company', null);
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text>Company {JSON.stringify(company.company_name)}</Text>
            </View>
        );
    }
}