import React from 'react';
// import fetch from 'fetch';
import {
    View,
    Text, ActivityIndicator, FlatList, TouchableOpacity,
} from 'react-native';

export class ListCompanies extends React.Component {
    static navigationOptions = {
        title: 'Divvy Companies',
    };

    constructor(props) {
        super(props);
        this.state = {isLoading: true}
    }

    componentDidMount() {
        fetch('https://my.api.mockaroo.com/company.json?key=b48b09c0', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        })
            .then(response => response.json())
            // .then(companies => {
            //     companies.sort((a, b) => (a.company_name > b.company_name) ? 1 : ((b.company_name > a.company_name) ? -1 : 0));
            //     return companies
            // })
            .then(companies => {
                console.log("*******" + JSON.stringify(companies));
                this.setState({
                    isLoading: false,
                    dataSource: companies,
                }, function () {

                });
            })
            .catch((error) => {
                console.error(error);
            });
    }

    _getCompany(id) {
        let companies = this.state.dataSource;
        for (let i = 0; i < companies.length; i++) {
            if (companies[i].id.toString() === id) {
                return companies[i];
            }
        }
        return null;
    }

    _onPressItem = (id) => {
        let company = this._getCompany(id);
        this.props.navigation.navigate('Company', {
            company: company
        })
    };

    _renderItem = ({item}) => (
        <MyListItem
            id={item.id.toString()}
            onPressItem={this._onPressItem}
            title={item.company_name}
        />
    );

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator/>
                </View>
            )
        }
        return (
            <View style={{flex: 1, paddingTop: 20}}>
                <FlatList
                    data={this.state.dataSource}
                    extraData={this.state}
                    renderItem={this._renderItem}
                    keyExtractor={({id}, index) => id.toString()}
                />
            </View>
        );
    }
}

class MyListItem extends React.PureComponent {
    _onPress = () => {
        this.props.onPressItem(this.props.id);
    };

    render() {
        const textColor = this.props.selected ? "red" : "black";
        return (
            <TouchableOpacity onPress={this._onPress}>
                <View>
                    <Text style={{color: textColor}}>
                        {this.props.title}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}